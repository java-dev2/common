package com.netflix.common.constants;

public enum Gender {
    MALE('M'),
    FEMALE('F');

    Gender(Character value) {
        this.value = value;
    }

    private final Character value;

    public static Gender findByValue(Character value) {
        for (Gender gender : Gender.values()) {
            if (gender.getValue().equals(value)) {
                return gender;
            }
        }
        throw new IllegalArgumentException();
    }

    public Character getValue() {
        return value;
    }
}
