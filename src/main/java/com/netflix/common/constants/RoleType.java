package com.netflix.common.constants;

public enum RoleType {
    EMPLOYEE("employee"),
    MANAGER("manager");
    private final String roleType;

    RoleType(String roleType) {
        this.roleType = roleType;
    }

    public String getRoleType() {
        return roleType;
    }
}
