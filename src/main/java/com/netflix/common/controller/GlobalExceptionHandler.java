package com.netflix.common.controller;

import com.netflix.common.aop.WrapperLoggerFactory;
import com.netflix.common.customize.exception.GlobalException;
import com.netflix.common.utils.Logging;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {
    private static final Logging LOGGING = WrapperLoggerFactory.getLogging();

    @ExceptionHandler
    public ResponseEntity handleException(Exception e) {
        LOGGING.error("HandleException " + e.getClass().getCanonicalName(), e);
        if (e instanceof BadCredentialsException || e instanceof UsernameNotFoundException) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        } else if (e instanceof AccessDeniedException) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        } else if (e instanceof GlobalException) {
            return ResponseEntity.status(((GlobalException) e).getHttpStatus()).body(((GlobalException) e).getBody());
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
