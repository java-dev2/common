package com.netflix.common.idm;

import com.netflix.common.aop.WrapperLoggerFactory;
import com.netflix.common.customize.exception.GlobalException;
import com.netflix.common.customize.security.NetflixUserDetail;
import com.netflix.common.dto.AuthorizationDto;
import com.netflix.common.utils.Logging;
import com.netflix.common.utils.UserIntegration;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.GenericFilter;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

public class AuthenticationAndAuthorizationFilter extends GenericFilter {
    private static final long serialVersionUID = -4555594859682657299L;
    private final UserIntegration userIntegration;
    private static final Logging LOGGING = WrapperLoggerFactory.getLogging();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        super.init(filterConfig);
    }

    public AuthenticationAndAuthorizationFilter(UserIntegration userIntegration) {
        this.userIntegration = userIntegration;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        String tokenId = httpServletRequest.getHeader(HttpHeaders.AUTHORIZATION);
        if (StringUtils.isNotBlank(tokenId)) {
            try {
                AuthorizationDto authorizationDto = userIntegration.getAuthorizationDto(tokenId, httpServletRequest.getRequestURI(), httpServletRequest.getMethod());
                Authentication netflixUser = new NetflixUserDetail(authorizationDto.getUserId(), null);
                SecurityContextHolder.getContext().setAuthentication(netflixUser);
                chain.doFilter(httpServletRequest, httpServletResponse);
            } catch (IOException | URISyntaxException | KeyManagementException | KeyStoreException | NoSuchAlgorithmException | ServletException e) {
                PrintWriter writer = httpServletResponse.getWriter();
                httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                writer.print(e);
                writer.flush();
            } catch (GlobalException e) {
                PrintWriter writer = httpServletResponse.getWriter();
                httpServletResponse.setStatus(e.getHttpStatus().value());
                writer.print(e.getBody());
                writer.flush();
            }
        } else {
            chain.doFilter(request, response);
        }
    }


}
