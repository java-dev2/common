package com.netflix.common.utils;

import org.owasp.esapi.Logger;

public class Logging {
    private Logger logger;

    private Logging(Logger logger) {
        this.logger = logger;
    }

    public static Logging getInstance(Logger logger) {
        return new Logging(logger);
    }

    public void info(String message) {
        logger.info(Logger.EVENT_UNSPECIFIED, message);
    }

    public void debug(String message) {
        logger.debug(Logger.EVENT_UNSPECIFIED, message);
    }

    public void warn(String message) {
        logger.warning(Logger.EVENT_UNSPECIFIED, message);
    }

    public void trace(String message, Throwable throwable) {
        logger.trace(Logger.EVENT_UNSPECIFIED, message, throwable);
    }

    public void error(String message) {
        logger.error(Logger.EVENT_UNSPECIFIED, message);
    }

    public void error(String message, Throwable throwable) {
        logger.error(Logger.EVENT_UNSPECIFIED, message, throwable);
    }

    public boolean isDebugEnabled() {
        return logger.isDebugEnabled();
    }
}
