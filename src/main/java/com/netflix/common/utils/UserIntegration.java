package com.netflix.common.utils;

import com.netflix.common.dto.AuthorizationDto;
import com.netflix.common.integration.ServiceIntegration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Component
public class UserIntegration {
    @Autowired
    private ServiceIntegration serviceIntegration;
    @Value("${netflix.permission.endpoint}")
    private String checkApiPermissionEndpoint;

    public AuthorizationDto getAuthorizationDto(String tokenId, String url, String method) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException, URISyntaxException {
        Map<String, String> headers = Collections.singletonMap(HttpHeaders.AUTHORIZATION, tokenId);
        Map<String, String> params = new HashMap<>();
        params.put("url", url);
        params.put("method", method);
        return serviceIntegration.integrated(checkApiPermissionEndpoint, HttpMethod.GET, headers, params, null, AuthorizationDto.class);
    }

}
