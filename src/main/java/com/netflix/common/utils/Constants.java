package com.netflix.common.utils;

public final class Constants {
    private Constants() {

    }

    public static final String BEARER_PREFIX = "Bearer ";
}
