package com.netflix.common.dto;

public class AuthorizationDto {
    private String userId;
    private String path;

    public AuthorizationDto(String userId, String path) {
        this.userId = userId;
        this.path = path;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public AuthorizationDto() {
    }
}
