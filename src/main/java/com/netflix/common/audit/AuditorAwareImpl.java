package com.netflix.common.audit;

import com.netflix.common.customize.security.NetflixUser;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AuditorAwareImpl implements AuditorAware<Long> {
    @Override
    public Optional<Long> getCurrentAuditor() {
        NetflixUser netflixUser = (NetflixUser) SecurityContextHolder.getContext().getAuthentication();
        return Optional.of(Long.parseLong(netflixUser.getUserId()));
    }
}
