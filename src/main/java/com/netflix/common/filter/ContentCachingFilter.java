package com.netflix.common.filter;

import com.netflix.common.aop.WrapperLoggerFactory;
import com.netflix.common.customize.ContentCachingHttpServletRequest;
import com.netflix.common.utils.Logging;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ContentCachingFilter implements Filter {
    private static final Logging LOGGING = WrapperLoggerFactory.getLogging();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //Default
        String contextPath = filterConfig.getServletContext().getContextPath();
        LOGGING.info(filterConfig.getFilterName() + " is running with contextPath " + contextPath);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
            HttpServletRequest httpServletRequest = (HttpServletRequest) request;
            HttpServletResponse httpServletResponse = (HttpServletResponse) response;
            ContentCachingHttpServletRequest contentCachingHttpServletRequest = new ContentCachingHttpServletRequest(httpServletRequest);
            String requestBody = contentCachingHttpServletRequest.getRequestBody();
            LOGGING.info(requestBody);
            chain.doFilter(contentCachingHttpServletRequest, httpServletResponse);
        } else {
            chain.doFilter(request, response);
        }
    }

}
