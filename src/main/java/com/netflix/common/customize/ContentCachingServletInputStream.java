package com.netflix.common.customize;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ContentCachingServletInputStream extends ServletInputStream {
    private final InputStream cachingInputStream;

    public ContentCachingServletInputStream(byte[] cachedRequestBody) {
        this.cachingInputStream = new ByteArrayInputStream(cachedRequestBody);
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public boolean isReady() {
        return false;
    }

    @Override
    public void setReadListener(ReadListener listener) {
        //unused
    }

    @Override
    public int read() throws IOException {
        return cachingInputStream.read();
    }
}
