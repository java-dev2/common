package com.netflix.common.customize.exception;

import org.springframework.http.HttpStatus;

public class GlobalException extends RuntimeException {
    private static final long serialVersionUID = -1651462333937070503L;
    private final HttpStatus httpStatus;
    private final String body;

    public GlobalException(HttpStatus httpStatus, String body) {
        this.httpStatus = httpStatus;
        this.body = body;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getBody() {
        return body;
    }
}
