package com.netflix.common.customize.exception;

public class InvalidFileNameException extends RuntimeException {

    private static final long serialVersionUID = 272743848269314562L;

    private final String fileName;

    public InvalidFileNameException(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }
}
