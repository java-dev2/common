package com.netflix.common.customize.exception;

public class MissingFileNameException extends RuntimeException {
    private static final long serialVersionUID = 5535678701726687896L;
}
