package com.netflix.common.customize;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.StreamUtils;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class ContentCachingHttpServletRequest extends HttpServletRequestWrapper {
    private final byte[] requestBody;

    /**
     * Constructs a request object wrapping the given request.
     *
     * @param request The request to wrap
     * @throws IllegalArgumentException if the request is null
     */
    public ContentCachingHttpServletRequest(HttpServletRequest request) throws IOException {
        super(request);
        ServletInputStream requestInputStream = request.getInputStream();
        this.requestBody = StreamUtils.copyToByteArray(requestInputStream);
    }

    @Override
    public ServletInputStream getInputStream() {
        return new ContentCachingServletInputStream(requestBody);
    }

    @Override
    public BufferedReader getReader() {
        InputStream byteArrayInputStream = new ByteArrayInputStream(this.requestBody);
        return new BufferedReader(new InputStreamReader(byteArrayInputStream));
    }

    public String getRequestBody() {
        return StringUtils.toEncodedString(this.requestBody, StandardCharsets.UTF_8);
    }
}
