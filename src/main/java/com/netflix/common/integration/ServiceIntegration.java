package com.netflix.common.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.common.aop.WrapperLoggerFactory;
import com.netflix.common.customize.exception.GlobalException;
import com.netflix.common.customize.http.NetflixHttpDelete;
import com.netflix.common.customize.http.NetflixHttpGet;
import com.netflix.common.utils.JsonConverter;
import com.netflix.common.utils.Logging;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

@Component
public class ServiceIntegration {
    @Autowired
    private JsonConverter jsonConverter;
    private static final int CONNECTION_TIMEOUT = 50000;
    private static final int CONNECTION_REQUEST_TIMEOUT = 50000;
    private static final Logging LOGGING = WrapperLoggerFactory.getLogging();

    private HttpClient getCloseableHttpClient() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(CONNECTION_TIMEOUT)
                .setConnectionRequestTimeout(CONNECTION_REQUEST_TIMEOUT)
                .build();
        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create()
                .setDefaultRequestConfig(requestConfig);
        SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null, (chain, authType) -> true).build();
        httpClientBuilder.setSSLContext(sslContext);
        HostnameVerifier hostnameVerifier = NoopHostnameVerifier.INSTANCE;
        SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(sslContext, hostnameVerifier);
        Registry<ConnectionSocketFactory> connectionSocketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", PlainConnectionSocketFactory.getSocketFactory())
                .register("https", sslConnectionSocketFactory)
                .build();
        PoolingHttpClientConnectionManager poolingHttpClientConnectionManager = new PoolingHttpClientConnectionManager(connectionSocketFactoryRegistry);
        httpClientBuilder.setConnectionManager(poolingHttpClientConnectionManager);
        return httpClientBuilder.build();
    }

    private HttpEntityEnclosingRequestBase getHttpRequest(String path, HttpMethod method) {
        switch (method) {
            case POST:
                return new HttpPost(path);
            case GET:
                return new NetflixHttpGet(path);
            case PUT:
                return new HttpPut(path);
            case DELETE:
                return new NetflixHttpDelete(path);
            default:
                throw new IllegalArgumentException(method.name());

        }
    }

    public <T> T integrated(String path, HttpMethod httpMethod, Map<String, String> headers, Map<String, String> params, Object requestBody, Class<T> returnType)
            throws IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, URISyntaxException {
        HttpEntityEnclosingRequestBase httpRequest = getHttpRequest(path, httpMethod);
        if (requestBody != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            StringEntity stringEntity = new StringEntity(objectMapper.writeValueAsString(requestBody), StandardCharsets.UTF_8);
            stringEntity.setContentEncoding(MediaType.APPLICATION_JSON_VALUE);
            httpRequest.setEntity(stringEntity);
        }
        if (!CollectionUtils.isEmpty(headers)) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                httpRequest.setHeader(entry.getKey(), entry.getValue());
            }
        }
        if (!CollectionUtils.isEmpty(params)) {
            URIBuilder uriBuilder = new URIBuilder(httpRequest.getURI());
            for (Map.Entry<String, String> entry : params.entrySet()) {
                uriBuilder.addParameter(entry.getKey(), entry.getValue());
            }
            httpRequest.setURI(uriBuilder.build());
        }
        HttpClient httpClient = getCloseableHttpClient();
        HttpResponse httpResponse = httpClient.execute(httpRequest);
        int statusCode = httpResponse.getStatusLine().getStatusCode();
        if (statusCode == HttpStatus.NO_CONTENT.value()) {
            return null;
        }
        InputStreamReader inputStreamReader = new InputStreamReader(httpResponse.getEntity().getContent());
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        LOGGING.info(stringBuilder.toString());
        if (statusCode != 200 && statusCode != 201) {
            HttpStatus httpStatus = HttpStatus.valueOf(statusCode);
            throw new GlobalException(httpStatus, stringBuilder.toString());
        }
        return jsonConverter.convertJsonToPojo(stringBuilder.toString(), returnType);
    }
}
